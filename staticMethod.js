class Human {
  static isLivingOnEarth = true;
  static group = "Vertebrate";
  constructor(name, isAlive) {
    this.name = name; // This will create a property called name
    this.isAlive = isAlive; //
  }
  // Instance method signature
  introduce() {
    console.log(
      `Hi, my name is ${this.name} and I'm ${this.isAlive ? "alive" : "dead"}`
    );
  }
  static isEating(food) {
    let foods = ["plant", "animal"];
    return foods.includes(food.toLowerCase());
  }
}
console.log(Human.isEating("Plant")); // true
console.log(Human.isEating("Human")); // false
