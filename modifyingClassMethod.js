class Human {
  constructor(name, address) {
    this.name = name;
    this.address = address;
  }
  introduce() {
    console.log(`Hi, my name is ${this.name}`);
  }
}

// Add prototype/instance method
Human.prototype.greet = function (name) {
  console.log(`Hi, ${name}, I'm ${this.name}`);
};
// Add static method
Human.destroy = function (thing) {
  console.log(`Human is destroying ${thing}`);
};

let mj = new Human("Michael Jackson", "Isekai");
mj.greet("Donald Trump"); // Hi, Donald Trump, I'm Michael Jackson
Human.destroy("Amazon Forest"); // Human is destroying Amazon Forest
