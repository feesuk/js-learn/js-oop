class Human {
  static isLivingOnEarth = true;
  static group = "Vertebrate";
  constructor(name, isAlive) {
    this.name = name; // This will create a property called name
    this.isAlive = isAlive; //
  }
  // Instance method signature
  introduce() {
    console.log(
      `Hi, my name is ${this.name} and I'm ${this.isAlive ? "alive" : "dead"}`
    );
    /*
    `this` keyword refers to the instance object. In this case we have an instance named `mj`, so it will return this thing
    Person { name: "Michael Jackson", isAlive: false } Which we can also call another method
    */
  }
}
// Instantiate
let mj = new Human("Michael Jackson", false);
mj.introduce(); // Output: Hi, my name is Michael Jackson and I'm dead
